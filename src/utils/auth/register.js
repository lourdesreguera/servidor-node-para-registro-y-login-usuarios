const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const User = require("../../api/users/user.model");
const { isValidEmail, isValidPassword } = require("../helpers/validators");

const saltRounds = 10;

const registerStrategy = new LocalStrategy(
  {
    usernameField: "email",
    passwordField: "password",
    passReqToCallback: true,
  },
  async (req, email, password, done) => {
    try {
      const existingUser = await User.findOne({ email: email.toLowerCase() });

      if (existingUser) {
        const error = new Error("El usuario ya existe");
        error.status = 401;
        return done(error, null);
      }

      if (!isValidEmail(email)) {
        const error = new Error("El email no es válido");
        return done(error, null);
      }

      if (!isValidPassword(password)) {
        const error = new Error("La contraseña no cumple las reglas. 8 carácteres, 1minúscula, 1 mayúscula y 1 número");
        return done(error, null);
      }

      const encryptedPassword = await bcrypt.hash(password, saltRounds);

      const user = new User({
        ...req.body,
        email,
        password: encryptedPassword,
      });

      const userDB = await user.save();

      userDB.password = "Contraseña protegida";

      return done(null, userDB);

    } catch (error) {
      return done(error.message);
    }
  }
);

module.exports = registerStrategy;
