const express = require("express");
const db = require("./src/utils/database/db");
const dotenv = require("dotenv");
const cors = require("cors");
const passport = require("passport");
require("./src/utils/auth/index");
const session = require("express-session");

// todos los usuarios con sesiones iniciadas.
const MongoStore = require("connect-mongo");

const mainRoutes = require("./src/api/main/main.routes");
const userRoutes = require("./src/api/users/user.routes");

dotenv.config();
db.connect();

const PORT = process.env.PORT || 5000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, PATCH, DELETE");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

app.use(cors({ origin: "http://localhost:3000", credentials: true }));

app.use(
  session({
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    resave: false,
    cookie: {
      maxAge: 2 * 60 * 60 * 1000,
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL }),
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use("/", mainRoutes);
app.use("/users", userRoutes);

app.use((error, req, res, next) => {
  const status = error.status || 500;
  const message = error.message || "Unexpected error";

  return res.status(status).json(message);
});

app.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});
